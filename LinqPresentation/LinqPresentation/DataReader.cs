﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqPresentation
{
    internal class DataReader
    {
        public string Path { get; private set; }

        public DataReader(string path)
        {
            if(File.Exists(path))
                 Path = path;
            else
            {
                throw new FileNotFoundException(path);
            }
        }

        
    }
}
